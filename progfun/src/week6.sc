object week6 {
  val l = List("a", "b", "c")                     //> l  : List[String] = List(a, b, c)
  val v = Vector("a", "b", "c")                   //> v  : scala.collection.immutable.Vector[String] = Vector(a, b, c)
  val a = Array("a", "b", "c")                    //> a  : Array[String] = Array(a, b, c)
  val s = "Hello World"                           //> s  : String = Hello World
  s filter (c => c.isUpper)                       //> res0: String = HW
  1 until 5                                       //> res1: scala.collection.immutable.Range = Range(1, 2, 3, 4)
  1 to 5                                          //> res2: scala.collection.immutable.Range.Inclusive = Range(1, 2, 3, 4, 5)
  1 to 10 by 3                                    //> res3: scala.collection.immutable.Range = Range(1, 4, 7, 10)
  6 to 1 by -2                                    //> res4: scala.collection.immutable.Range = Range(6, 4, 2)
  (1 to 10) filter (i => i < 5)                   //> res5: scala.collection.immutable.IndexedSeq[Int] = Vector(1, 2, 3, 4)

  def isPrime(n: Int): Boolean = (2 until n) forall (d => n % d != 0)
                                                  //> isPrime: (n: Int)Boolean

  List(1, 2, 3, 4, 5) map (isPrime)               //> res6: List[Boolean] = List(true, true, true, false, true)

  val n = 7                                       //> n  : Int = 7
  ((1 until n) flatMap (i =>
    (1 until i) map (j => (i, j))) filter { case (i, j) => isPrime(i + j) })
                                                  //> res7: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((2,1), (3,2
                                                  //| ), (4,1), (4,3), (5,2), (6,1), (6,5))
  for {
    i <- 1 until n
    j <- 1 until i
    if isPrime(i + j)
  } yield (i, j)                                  //> res8: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((2,1), (3,2
                                                  //| ), (4,1), (4,3), (5,2), (6,1), (6,5))

  def scalarProduct(xs: List[Double], ys: List[Double]): Double =
    (for ((x, y) <- xs zip ys) yield x * y).sum   //> scalarProduct: (xs: List[Double], ys: List[Double])Double

	
}