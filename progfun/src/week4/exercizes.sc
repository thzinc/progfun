package week4

object exercizes {
  println("woo")                                  //
                                                  //> woo
  val x = Zero                                    //> x  : week4.Zero.type = 0
  val y = new Succ(x)                             //> y  : week4.Succ = 1
  val z = new Succ(y)                             //> z  : week4.Succ = 2
  z + z                                           //> res0: week4.Succ = 4
}

class IntSet {

}

object Empty extends IntSet {

}

class NonEmpty(x: Int, left: IntSet, right: IntSet) extends IntSet {

}

// Provide an implementation of the abstract class Nat that represents non-negative integers.

abstract class Nat {
  def isZero: Boolean
  def predecessor: Nat
  def successor: Nat = new Succ(this)
  def +(that: Nat): Nat
  def -(that: Nat): Nat
}

// Do not use standard numerical classes in this implementation. Rather, implement a sub-object and a sub-class:

object Zero extends Nat {
  def isZero = true
  def predecessor = throw new Error()
  def +(that: Nat) = that
  def -(that: Nat) = if (that.isZero) this else throw new Error()

  override def toString = "0"
}
class Succ(n: Nat) extends Nat {
  def isZero = false
  def predecessor = n
  def +(that: Nat) = new Succ(n + that)
  def -(that: Nat) = if (that.isZero) this else n - that.predecessor
  private def count(nat: Nat, acc: Int): Int = if (nat.predecessor.isZero) acc + 1 else count(nat.predecessor, acc + 1)
  override def toString = count(this, 0).toString
}