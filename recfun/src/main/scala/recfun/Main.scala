package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if (r < 0) 0
    else if (c < 0) 0
    else if (c > r) 0
    else if (c == 0) 1
    else if (r == c) 1
    else pascal(c, r - 1) + pascal(c - 1, r - 1) 
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    def loop(count: Int, chars: List[Char]): Int = {
      if (count < 0 || chars.isEmpty) count
      else loop(count + {
        chars.head match {
          case '(' => 1
          case ')' => -1
          case _ => 0
        }
      }, chars.tail)
    }

    loop(0, chars) == 0
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    def count(n: Int, coins: List[Int]): Int = {
      if (n == 0) 1
      else if (n < 0) 0
      else if (coins.isEmpty) 0
      else count(n, coins.tail) + count(n - coins.head, coins)
    }
    
    if (money <= 0) 0
    else count(money, coins)
  }
}
