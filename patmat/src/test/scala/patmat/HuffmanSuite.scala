package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {
  trait TestTrees {
    val t1 = Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5)
    val t2 = Fork(Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5), Leaf('d',4), List('a','b','d'), 9)
    
    val plaintext = "the cat is black"
    val codeTree = createCodeTree(plaintext.toList)
  }

  test("weight of a larger tree") {
    new TestTrees {
      assert(weight(t1) === 5)
    }
  }

  test("chars of a larger tree") {
    new TestTrees {
      assert(chars(t2) === List('a','b','d'))
    }
  }

  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }

  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e',1), Leaf('t',2), Leaf('x',3)))
  }

  test("combine of some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leaflist) === List(Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3), Leaf('x',4)))
  }

  test("decode and encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
      assert(decode(codeTree, encode(codeTree)(plaintext.toList)).mkString === plaintext)
      assert(decode(codeTree, quickEncode(codeTree)(plaintext.toList)).mkString === plaintext)
    }
  }
  
  test("character frequency") {
    new TestTrees {
      assert(times(plaintext.toList) === List(('t',2), ('h',1), ('e',1), (' ',3), ('c',2), ('a',2), ('i',1), ('s',1), ('b',1), ('l',1), ('k',1)))
    }
  }	
  
  test("createCodeTree") {
    new TestTrees {
      val (chars, weight) = codeTree match {
        case Fork(_, _, chars, weight) => (chars, weight)
      }
      
      assert(weight === 16)
      assert(chars.length == 11)
    }
  }
  
  test("decode secret") {
    new TestTrees {
      assert(decodedSecret.mkString === "huffmanestcool")
    }
  }
}
