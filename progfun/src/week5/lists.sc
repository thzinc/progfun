package week5

object lists {
  def flatten(xs: List[Any]): List[Any] = xs match {
  	case Nil => xs
  	case (y :: ys) :: zs => y :: flatten(ys) ::: flatten(zs)
  	case y :: ys => y :: flatten(ys)
  }                                               //> flatten: (xs: List[Any])List[Any]

  flatten(List(List(1, 1), 2, List(3, List(5, 8))))
                                                  //> res0: List[Any] = List(1, 1, 2, 3, 5, 8)
                                                  
                                                  
  val a = 1 :: Nil                                //> a  : List[Int] = List(1)
  a == List(1)                                    //> res1: Boolean = true
  Nil.::(1) == a                                  //> res2: Boolean = true
}