package week3

object rationals {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(60); 
	val x = new Rational(1,3);System.out.println("""x  : week3.Rational = """ + $show(x ));$skip(27); 
	val y = new Rational(5,7);System.out.println("""y  : week3.Rational = """ + $show(y ));$skip(27); 
	val z = new Rational(3,2);System.out.println("""z  : week3.Rational = """ + $show(z ));$skip(13); val res$0 = 
	
	x - y - z;System.out.println("""res0: week3.Rational = """ + $show(res$0));$skip(7); val res$1 = 
	y + y;System.out.println("""res1: week3.Rational = """ + $show(res$1));$skip(47); 
	
	var a = new Rational(2000000000,1000000000);System.out.println("""a  : week3.Rational = """ + $show(a ));$skip(7); val res$2 = 
	a + a;System.out.println("""res2: week3.Rational = """ + $show(res$2))}
}

class Rational(x: Int, y: Int) {
	require(y != 0, "denominator must be nonzero")
	
	def this(x: Int) = this(x, 1)
	
	private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
	private val g = gcd(x,y)
  def numer = x/g
  def denom = y/g
  
  def < (that: Rational) = numer * that.denom < that.numer * denom
  
  def max(that: Rational) = if (this < that) this else that

  def +(that: Rational) =
    new Rational(
      numer * that.denom + that.numer * denom,
      denom * that.denom)

  def unary_- = new Rational(-numer, denom)

  def - (that: Rational) = this + -that

  override def toString = numer + "/" + denom
}
