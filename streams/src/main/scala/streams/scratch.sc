package streams

import common._
import Bloxorz._

object scratch {
  Level0.solution                                 //> res0: List[streams.Bloxorz.Level0.Move] = List(Down, Right, Up)
  Level1.solution                                 //> res1: List[streams.Bloxorz.Level1.Move] = List(Right, Right, Down, Right, Ri
                                                  //| ght, Right, Down)

  val x = Stream("blah", "foo") ++ Stream("bar")  //> x  : scala.collection.immutable.Stream[String] = Stream(blah, ?)
  val y = Stream("blah", "foo", "bar")            //> y  : scala.collection.immutable.Stream[String] = Stream(blah, ?)
  x == y                                          //> res2: Boolean = true
}