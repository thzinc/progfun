package funsets

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * This class is a test suite for the methods in object FunSets. To run
 * the test suite, you can either:
 *  - run the "test" command in the SBT console
 *  - right-click the file in eclipse and chose "Run As" - "JUnit Test"
 */
@RunWith(classOf[JUnitRunner])
class FunSetSuite extends FunSuite {


  /**
   * Link to the scaladoc - very clear and detailed tutorial of FunSuite
   *
   * http://doc.scalatest.org/1.9.1/index.html#org.scalatest.FunSuite
   *
   * Operators
   *  - test
   *  - ignore
   *  - pending
   */

  /**
   * Tests are written using the "test" operator and the "assert" method.
   */
  test("string take") {
    val message = "hello, world"
    assert(message.take(5) == "hello")
  }

  /**
   * For ScalaTest tests, there exists a special equality operator "===" that
   * can be used inside "assert". If the assertion fails, the two values will
   * be printed in the error message. Otherwise, when using "==", the test
   * error message will only say "assertion failed", without showing the values.
   *
   * Try it out! Change the values so that the assertion fails, and look at the
   * error message.
   */
  test("adding ints") {
    assert(1 + 2 === 3)
  }

  
  import FunSets._

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }
  
  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   * 
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   * 
   *   val s1 = singletonSet(1)
   * 
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   * 
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   * 
   */

  trait TestSets {
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
  }

  /**
   * This test is currently disabled (by using "ignore") because the method
   * "singletonSet" is not yet implemented and the test would fail.
   * 
   * Once you finish your implementation of "singletonSet", exchange the
   * function "ignore" by "test".
   */
  test("singletonSet(1) contains 1") {
    
    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3". 
     */
    new TestSets {
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "Singleton")
    }
  }

  test("union contains all elements") {
    new TestSets {
      val s = union(s1, s2)
      assert(contains(s, 1), "Union 1")
      assert(contains(s, 2), "Union 2")
      assert(!contains(s, 3), "Union 3")
    }
  }
  
  test("intersect contains all intersected elements") {
    new TestSets {
      val s = union(s1, s2)
      val t = intersect(s, s2)
      assert(contains(t, 2), "Intersect 2")
      assert(!contains(t, 1), "Intersect 1")
    }
  }
  
  test("diff contains all different elements") {
    new TestSets {
      val s = union(s1, s2)
      val t = diff(s, s3)
      assert(contains(t, 1), "Diff 1")
      assert(contains(t, 2), "Diff 2")
      assert(!contains(t, 3), "Diff 3")
    }
  }
  
  test("filter contains only filtered elements") {
    new TestSets {
      val s = union(union(s1, s2), s3)
      val f = filter(s, x => x > 1)
      assert(!contains(f, 1), "Filter 1")
      assert(contains(f, 2), "Filter 2")
      assert(contains(f, 3), "Filter 3")
    }
  }
  
  test("are all members of the set..") {
    new TestSets {
      val s = union(union(s1, s2), s3)
      assert(forall(s, x => x > 0), "All greater than zero")
      assert(forall(s, x => x < 4), "All less than four")
      assert(!forall(s, x => x == 1), "All are equal to 1")
    }
  }
  
  test("are at least one member of the set...") {
    new TestSets {
      val s = union(union(s1, s2), s3)
      assert(exists(s, x => x > 2), "At least one element greater than 2")
      assert(!exists(s, x => x == 4), "At least one element equal to 4")
    }
  }
  
  test("map set to another") {
    new TestSets {
      assert(map(s2, x => x * x)(4), "Singleton set mapped to x^2")
      
      val s = union(union(s1, s2), s3)
      val m = map(s, x => x + 3)
      assert(forall(m, x => x >= 4 && x <= 6), "Set is between 4 and 6, inclusive")
      
      val u = union(s, m)
      assert(forall(u, x => x >= 1 && x <= 6), "Set is between 1 and 6, inclusive")
      
      val autograder: Set = x => x == 1 || (x >= 3 && x <= 5) || x == 7 || x == 1000
      assert(autograder(1), "autograder contains 1")
      assert(autograder(3), "autograder contains 3")
      assert(autograder(4), "autograder contains 4")
      assert(autograder(5), "autograder contains 5")
      assert(autograder(7), "autograder contains 7")
      assert(autograder(1000), "autograder contains 1000")
      
      var mappedAutograder = map(autograder, x => x - 1)
      assert(mappedAutograder(0), "mappedAutograder contains 0")
      assert(mappedAutograder(2), "mappedAutograder contains 2")
      assert(mappedAutograder(3), "mappedAutograder contains 3")
      assert(mappedAutograder(4), "mappedAutograder contains 4")
      assert(mappedAutograder(6), "mappedAutograder contains 6")
      assert(mappedAutograder(999), "mappedAutograder contains 999")
      assert(!mappedAutograder(1000), "mappedAutograder does not contain 1000")
    }
  }
}
